import VueRouter from 'vue-router'
import HelloWorld from './components/HelloWorld.vue'

const routes = [
  { path: '/', component: HelloWorld, props: {msg: 'This is a test'} }
];

export default new VueRouter({
  routes
})
